# Passos para instalar o arch linux + interface xfce4

## Resumo
Essa guia tem como objectivo apresentar passo a passo de como instalar o arch linux com a interface grafica xfce4 num computador.

#### Sobre
* Author:	Daniel Costa
* Trabalho:	JIGAsoft HD, LTD
* Country:	São Tomé e Principe

### Requerimentos
* `archlinux` (arch linux instalado)
* `xfce4`
* `xorg`
	* Item 1
	* Item 2
	* Item 3


### Detalhes da instalação
* OS: ArchLinux 
* DE: xfce4



**Important:** Remove all older versions of the theme from your system before you proceed any further

# Instalação
## [ambiente]
	loadkeys pt-latin9
	ping 1.1.1.1

## [ambiente-opcional-wifi]
Wifi para o caso de portatil e a instalação ser em wifi*
	iwconfig						# Listar as interfaces disponivel
	wifi-menu $nomePlacaWifi		# Conectar a rede wifi subsitituir o $nomePlacaWifi pelo real nome da placa de rede


## [partition]
	lsblk							# Listar os discos disponivel
	cfdisk 							# Escolher a opção (dos) do cfdisk para criar a partição do disco
	#									Recomendo criar pelo menos tres partições
	#									sda1 (/) 		Minimo: 800MB base + 3GB Interface | 			Recomendado: 8GB minimo 	| Primaria 	| Linux ext4
	#									sda2 (swap)		Recomendado: 2GB												| Primaria	| Swap
	#									sda3 (/home)	Restante | Ou deixar uma parte para criar as 	partições logicas	| Primaria	| Linux ext4
	#									sda4 (extendida) opcional
										sda5, sda6, sda7 ... sda255
	mkfs.ext4 /dev/sda${r|h} 		# ${r|h} corresponde as partições do tipo linux
	mkswap /dev/sda${s}				# ${s} corresponde as partições do tipo swap

	swapon /dev/sda${s}				# ${s} corresponde as partições do tipo swap
	mount /dev/sda${r} /mnt			# ${r} corresponde a partição pricipal (partição butavel)
	mkdir /mnt/home					# para criar a pasta home onde sera montado a partição do home
	mount /dev/sda${h} /mnt/home	# ${h} corresponde a partição criada para o home



## [mirrorlist]
nano /etc/pacman.d/mirrorlist	# Enter mirror file via nano
#	1º Escolha na lista o mirror preferido (isso vai melhorar a velociado de dowload do pacote)
#		# em são tome é mais viavel usar os mirror de portugal, espanha ou frança
#	2º ctrl + k (para cortar o mirror preferido)
#	3º ctrl + u (para repor o mirror cortado)
#	4º mova para o top da lista do  mirror
#	5º ctrl + u (para colar o mirror preferido no top)


[arch-instalation]
pacstrap /mnt base base-devel				# Instalar o basico do sistema do ficheiro do arch linux no disco duro
#											O base-devel são as outras aplicações necessarias para o utilzar (tar, gcc, gc)
genfstab -U -p /mnt >> /mnt/etc/fstab		# Salvar as configurações de montagem das partições do mnt no final do arquivo /mnt/etc/fstab
#												Obtem as configurações de montagem atual e coloca no /mnt/etc/fstab
#												Isso fara os disco serem montado automatocamente na incialização do sistema
cat /mnt/etc/fstab							# Opcional é só para verificar como esta a montagem do disco
arch-chroot /mnt /bin/bash					# Entrar no arch instalado

[configsFinalAmbients]
## Fuso horario
ln -sf /usr/share/zoneinfo/($continent)/($country) /etc/localtime	# Para escolher o fuzo horario
hwclock --systohc													# Sincronizar a hora com o fuzo horario escolhido

## Linguagem base do sistema
nano /etc/locale.gen												# Abir o arquivo para configurar o idioma do sistema
#																		Procurar o idioma preferido e retirar do comentario (no caso en_US.UTF-8 UTF-8 para ingluês do estados unidos)						
echo "LANG=en_US.UTF-8" >> /etc/locale.conf							# Para adicionar linha de linguagem do sistema escolhido na configuração
locale-gen															# Para aplicar o idioma selecionado

## Definir o nome do computador 
echo "${hostName}" > /etc/hostname									# Definir o nome do computador subistitur ${hostName} pelo real nome do computador

## Configurar o ficheoiro host
nano /etc/hosts														# Editar o ficheiro /etc/hosts adicionando as seguintes linhas
++begin::lines

127.0.0.1			localhost.localdomanin		localhost
127.0.0.1			${hostName}.localdomain		${hostName}			# ${hostName} corresponde ao nome do computador
::1					localhost.localdomain		localhost
++lines::end

## Ativar o serviço de dhcp
systemctl enable dhcpcd

## Ativar o pacote multilib para o pacman
nano /etc/pacman.conf											# Retivar o comentario das linha com [multilib] e da linha segunite
pacman -Syyu 													# Atualizar o sistema por completo

[utilizadores]
## Adcionar o utilizador principal
useradd -m -g users -G wheel,storage,power -s /bin/bash ${userName}		# Subistituir o userName pelo nome do utilizador
passwd ${userName}														# Para definir a senha do utilizador principal
passwd																	# definir a senha para o root
nano /etc/sudoers														# Descomente a linha #%wheel ALL=(ALL) ALL
#																			para tornar o utilizador como sudo


[notebook-opcional]
## Pacotes para conexão com internet via wifi
pacman -S wireless_tools wpa_supplicant wpa_actiond dialog

## Pacotes para controle de bateria em notebook
pacman -S acpi acpid

[extras-opcioonal]
pacman -S screenfetch																# Para instalar o screenfetch (Apresenta o logo de OS no terminal)
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch; fi" >> /etc/bash.bashrc		# Para execuar o screenfetch com o bath em todos os utilizadores


[grub]
pacman -S grub os-prober					# Para instalar o grub 
#												os-prober é para o caso de pretender instalar os dual boot
grub-install /dev/sda						# Instalar o grub no sda
grub-mkconfig -o /boot/grub/grub.cfg		# Gerar e aplicar as configurações do grub


[finalizeInatalation]
exit					# Sair arch previa intalado
umount -R /mnt			# Desmontar todas as partições em /mnt
reboot 					# Reniciar o sistema operativo, se for na maquina virtual depois que reniciar tem que remover o iso



[pos-instalation]
## Xorg
pacman -S xorg-xinit xorg-server xorg-apps
pacman -S xorg-twm xorg-xclock

## Controlador de video 
pacman -S xf86-video-vesa

# ferramentas para rede
pacman -S networkmanager									# Instalar a feremante de gerenciamento de rede
systemctl enable NetworkManager

# ferramenta para monitoramento do recurso 
pacman -S htop

# ferramentas de audio
pacman -S alsa-utils



# Yaourt (muito importante)
sudo pacman -S git wget 									# Instalar o git e o wget
git clone https://aur.archlinux.org/package-query.git		# Clonar o repositorio do projecto package-query uma dependencia para o Yaourt
cd package-query/											# Mover para o projecto de package-query
makepkg -si													# Construir e instalar o projecto
cd ..														# Sair do projecto
git clone https://aur.archlinux.org/yaourt.git				# Clone o repositorio do projecto yaourt
cd yaourt/													# Entar no projecto yaourt
makepkg -si 												# Contruir e instalar o projecto yaourt
cd ..														# Sair do projecto
sudo rm -dR yaourt/ package-query/							# Remover os dois projecto clonados uma vez já instalado
yaourt -Syyu												# Atualizar o o pacote yaourt

## Instalação do google chrome
yaourt -S google-chrome										# Selecione as opção (N) quando for solicidado se pretennde editar os scripts



[pos-instalation-interface-grafica]
## Interface grafica
sudo pacman -S xfce4 xfce4-goodies network-manager-applet ttf-dejavu

## Icon papiros
sudo pacman -S papirus-icon-theme

## Gestor de arquivos
sudo pacman -S engrampa										# Utilizando o engrampa para gerenciar arquivos compactos


sudo pacman -S xfwm4-themes

yaourt -S vertex-themes

## Them suport 
yaourt -S ocs-url 											# Instalador do thema



[bluetooth-opcioanl]
pacman -S blueman

[pos-instalation-virtualbox]
pacman -S virtualbox-guest-utlis


[pos-extra-uninstal]
sudo pacman -Rcnsu xfburn				# Aplicação para gravar cd
sudo pacman -Rcnsu parole				# Reproduto de video default
sudo pacman -Rcnsu xfce4-appfinder		# Acho disnecessario sabendo que a aplicação do menu já faz isso da vida
sudo pacman -Rcnsu xfce4-dict			# Acho que deve ser utilizador que deve dicidir em instalar o dicionario ou não
sudo pacman -Rcnsu xfce4-screenshoter	# Esse aqui pode ser subistituido pelo flame shoot que é muito melhor simples e acessivel



[resources]
wget https://cn.pling.com/img//hive/content-pre1/122301-1.png		# icon lanucher








																														



## Creditos
# https://www.youtube.com/watch?v=nVewKKlaGQc															# Instalação do arch linux
# https://www.youtube.com/watch?v=zGMJSKuhfJ4&list=PLk9XvfPR8vS_Xm7E1D0zpVp3pc_nx_fLG&index=3&t=0s		# Instalação do arch linux
# https://angeloocana.com/pt/blog/linux/arch/install-arch-linux-wifi-xfce4/								# Instalação do arch linux
# https://www.ostechnix.com/install-arch-linux-latest-version/											# Instalação do arch linux
# https://wiki.manjaro.org/index.php/Install_Desktop_Environments										# serviço para a interface grafica
# https://www.ostechnix.com/install-yaourt-arch-linux/													# instalação do yaourt

## Outros creditos inportantes
# https://null-byte.wonderhowto.com/how-to/exploring-kali-linux-alternatives-set-up-ultimate-beginner-arch-linux-hacking-distro-with-manjaro-blackarch-0181782/
# Partições primarias e extendidas
# 	https://elias.praciano.com/2015/11/particoes-primarias-e-particoes-logicas/
## Screefetch instalation
# https://www.lifewire.com/show-system-information-terminal-with-screenfetch-2201159
## Screefetch no comando clear
# https://askubuntu.com/questions/952732/getting-screenfetch-to-run-after-clearing-terminal

## Thema 
# https://www.xfce-look.org/p/1013757/
# https://manjarobrasil.wordpress.com/2016/03/28/instalando-tema-vertex-no-manjaro/
## Thema::wiskermenu
# https://gottcode.wordpress.com/2017/10/21/theming-whisker-menu-redux/


## Salvar as configurações do XFCE4
# https://unix.stackexchange.com/questions/353924/how-to-copy-all-my-xfce-settings-between-a-desktop-machine-and-a-laptop


## Substitur thunar pelo pcmanfm
# https://forum.antergos.com/topic/5726/using-pcmanfm-with-xfce/8
# https://askubuntu.com/questions/303734/how-to-replace-thunar-with-pcmanfm

## Configuração de cor do terminal
# https://askubuntu.com/questions/123268/changing-colors-for-user-host-directory-information-in-terminal-command-prompt
https://makandracards.com/makandra/1090-customize-your-bash-prompt




